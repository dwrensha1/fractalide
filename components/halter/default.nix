{ stdenv, buildFractalideComponent, genName, upkeepers, ...}:

buildFractalideComponent rec {
  name = genName ./.;
  src = ./.;
  contracts = [];
  depsSha256 = "1lwc20psdf1pmlii0hx7vnmb69lihp1v14ragni7g3pk4vsv1l1w";

  meta = with stdenv.lib; {
    description = "Component: Keep the graph running";
    homepage = https://github.com/fractalide/fractalide/tree/master/components/ip/clone;
    license = with licenses; [ mpl20 ];
    maintainers = with upkeepers; [ dmichiels sjmackenzie];
  };
}
